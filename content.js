export let init_msgs = [
"[    1.361028] Freeing initrd memory: 28340K",
"[    1.361178] PCI-DMA: Using software bounce buffering for IO (SWIOTLB)",
"[    1.361856] Initialise system trusted keyrings",
"[    1.361873] Key type blacklist registered",
"[    1.364106] zbud: loaded",
"[    1.364409] integrity: Platform Keyring initialized",
"[    1.364414] Key type asymmetric registered",
"[    1.364416] Asymmetric key parser 'x509' registered",
"[    1.364428] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 251)",
"[    1.364507] io scheduler mq-deadline registered",
"[    1.365797] shpchp: Standard Hot Plug PCI Controller Driver version: 0.4",
"[    1.366788] ACPI: Thermal Zone [TZ00] (28 C)",
"[    1.367140] ACPI: Thermal Zone [TZ01] (30 C)",
"[    1.367413] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled",
"[    1.368736] Linux agpgart interface v0.103",
"[    1.368794] AMD-Vi: AMD IOMMUv2 functionality not available on this system - This is not a bug.",
"[    1.369095] i8042: PNP: No PS/2 controller found.",
"[    1.369183] mousedev: PS/2 mouse device common for all mice",
"[    1.382964] Segment Routing with IPv6",
"[    1.383003] mip6: Mobile IPv6",
"[    1.383006] NET: Registered protocol family 17",
"[    1.383548] registered taskstats version 1",
"[    1.383552] Loading compiled-in X.509 certificates",
"[    1.449507] zswap: loaded using pool lzo/zbud",
"[    1.449745] Key type .fscrypt registered",
"[    1.449746] Key type fscrypt-provisioning registered",
"[    1.449827] AppArmor: AppArmor sha1 policy hashing enabled",
"[    1.920414] ahci 0000:00:1f.2: version 3.0",
"[    1.920719] ahci 0000:00:1f.2: AHCI 0001.0300 32 slots 4 ports 6 Gbps 0x1 impl SATA mode",
"[    1.920723] ahci 0000:00:1f.2: flags: 64bit ncq led clo pio slum part ems apst",
"[    1.922169] r8169 0000:02:00.0 eth0: RTL8168g/8111g, 54:a0:50:d6:c4:2b, XID 4c0, IRQ 25",
"[    1.922173] r8169 0000:02:00.0 eth0: jumbo features [frames: 9194 bytes, tx checksumming: ko]",
"[    1.936191] r8169 0000:02:00.0 enp2s0: renamed from eth0",
"[    2.144651] usb 1-1: new high-speed USB device number 2 using ehci-pci",
"[    2.172653] usb 4-1: new high-speed USB device number 2 using ehci-pci",
"[    2.239453] ata1: SATA link up 6.0 Gbps (SStatus 133 SControl 300)",
"[    2.242569] ata1.00: configured for UDMA/133",
"[    2.258152] sd 0:0:0:0: [sda] 3907029168 512-byte logical blocks: (2.00 TB/1.82 TiB)",
"[    2.258156] sd 0:0:0:0: [sda] 4096-byte physical blocks",
"[    2.258179] sd 0:0:0:0: [sda] Write Protect is off",
"[    2.258182] sd 0:0:0:0: [sda] Mode Sense: 00 00 00 00",
"[    2.258361] sd 0:0:0:0: [sda] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA",
"[    2.309764]  sda: sda1 sda2 sda3 sda4",
"[    2.321697] sd 0:0:0:0: [sda] Attached SCSI disk",
"[    7.026598] EXT4-fs (sda3): mounted filesystem with ordered data mode. Opts: (null)",
"[    8.431494] random: crng init done",
"[    9.318266] fuse: init (API version 7.32)",
"[   10.717154] input: Power Button as /devices/LNXSYSTM:00/LNXSYBUS:00/PNP0C0C:00/input/input0",
"[   10.717199] ACPI: Power Button [PWRB]",
"[   10.717283] input: Power Button as /devices/LNXSYSTM:00/LNXPWRBN:00/input/input1",
"[   10.733479] ACPI: Power Button [PWRF]",
"[   10.996801] input: PC Speaker as /devices/platform/pcspkr/input/input2",
"[   11.004471] iTCO_vendor_support: vendor-support=0",
"[   11.016722] at24 0-0050: supply vcc not found, using dummy regulator",
"[   11.018004] at24 0-0050: 256 byte spd EEPROM, read-only",
"[   11.023123] iTCO_wdt: Intel TCO WatchDog Timer Driver v1.11",
"[   11.023181] iTCO_wdt: Found a Panther Point TCO device (Version=2, TCOBASE=0x0460)",
"[   11.024250] iTCO_wdt: initialized. heartbeat=30 sec (nowayout=0)",
"[   11.078261] sd 0:0:0:0: Attached scsi generic sg0 type 0",
"[   11.245823] cryptd: max_cpu_qlen set to 1000",
"[   11.388300] input: Eee PC WMI hotkeys as /devices/platform/eeepc-wmi/input/input3",
"[   11.593092] Console: switching to colour dummy device 80x25",
"[   11.648724] [drm] Initialized i915 1.6.0 20200917 for 0000:00:02.0 on minor 0",
"[   13.904685] Adding 1000444k swap on /dev/sda4.  Priority:-2 extents:1 across:1000444k FS",
"[   16.448911] EXT4-fs (sda1): mounted filesystem with ordered data mode.",
"[   16.519482] Done.",
"",
""
];


export let legals = [
"*{red#Mentions légales}*",
"Ce site internet est distribué sous la licence {gold#Creative Commons BY-SA}.",
"",
"Cette licence, permissive, permet à tout un chacun de réutiliser, modifier, redistribuer sous quelconque support une copie de ce projet (également à des fins commerciales) à condition de *créditer* son auteur et de publier la nouvelle oeuvre *sous la même licence*.",
"",
"",
"Plus d'informations sur le site de Creative Commons :",
" - Code légal : [version française](https://creativecommons.org/licenses/by-sa/4.0/legalcode.fr) - [english version](https://creativecommons.org/licenses/by-sa/4.0/legalcode.en).",
" - Acte légal : [version française](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) - [english version](https://creativecommons.org/licenses/by-sa/4.0/).",
"",
"",
"{blue#Auteur :} {white#BARDON Nathan}",
"{blue#Contact :} [{white#contact@lazyfox.fr}](mailto:contact@lazyfox.fr)",
"{blue#Année :} {white#2023}",
"",
"---",
"Utilisation de ressources externes :",
" - {blue#Svelte :} Licence [MIT](https://opensource.org/license/mit/). Voir leur [repository Github](https://github.com/sveltejs/svelte) pour les informations sur les contributeurs.",
" - {blue#Font NotoSansMono :} Licence [OFL](http://scripts.sil.org/OFL). Voir leur [repository Github](https://github.com/notofonts/latin-greek-cyrillic) pour les informations sur les contributeurs.",
"",
"---",
"Données collectées :",
" - Horodatage de la connexion au site internet.",
" - Adresse IP de connexion.",
" - User agent fourni par le navigateur.",
" - Pages visitées.",
"",
"Aucun cookie n'est déposé sur les navigateurs.",
"Les informations sont collectée pour les uniques buts de journalisation, de détection d'intrusion ainsi qu'à des fins légales. Elles ne sont jamais partagées à des tiers et n'ont aucune autre utilité que purement technique."
];


export let tree = {
	"_prompt": "Entrer une catégorie pour en découvrir davantage :",

	"Compétences": {
		"_text": "Principalement de {blue#l'informatique}... Mais pas que ! Mes compétences sont sans cesses renouvelées via une veille sur [TLDR](https://tldr.tech), [Medium](https://medium.com) ou d'autres plus petits blogs.",
		"_prompt": "Choisissez le domaine d'activité :",

		"Développement": {
			"_text": "Maîtrisant mes infrastructures de la première ligne de code au monitoring en passant par la mise en production, je tente d'intégrer la philosophie {blue#DevOPS} dans mes projets. Même si son implémentation n'est pas parfaite, je ne rate pas l'occasion d'un nouveau projet pour mettre en pratique les nouveaux concepts que j'intègre régulièrement.",
			"Web Frontend": {
				"_text": "Je développe régulièrement avec {red#HTML}, {blue#CSS} et {gold#Javascript} tout en assurant une veille pour connaître les dernières technologies. Pour les projets plus complexes, j'utilise le plus souvent le framwork [Svelte](https://svelte.dev) qui assure, entre autres, la réactivité et les one file components.",
				"Voir les projets web": { "_goto": "/Projets/Web" }
			},
			"Web Backend": {},
			"Applicatif": {},
			"Scripting": "Maîtrise du Bash pour le scripting Linux et Powershell côté Windows. Pour les scripts plus avancés, il peut m'arriver de les écrire en Javascript (avec NodeJS ou Bun) voire en Python lorsque c'est imposé."
		},

		"Admin. système": {
			"_prompt": "Choisissez une spécialité :",

			"Hashicorp Nomad": "Installation, configuration et utilisation de {blue#Nomad} d'Hashicorp. Il s'agit d'un orchestrateur d'applicatif Docker, Java, natif chrooté ou autre via des drivers additionnels. Mon infrastructure personnel actuelle repose sur des applicatifs redondées grâce à un cluster Nomad, exécuté sur des serveurs hétérogènes en terme de ressources, FAI, positionnement géographique, hébergeur...",
			"Linux": "Maîtrise avancée de l'administration d'un OS Linux, principalement [Debian](https://debian.org) et ses dérivés.",
			"Microsoft Windows": "Configuration et administration simple d'une infrastructure Windows et d'un Active Directory.",
			"Monitoring": "Utilisation de solutions de monitoring telles que [Nagios](https://nagios.com) ou [Prometheus](https://prometheus.io).",

			"Services linux": {
				"_text": "Installation, configuration et administration de divers services linux.",
				"_prompt": "Choisissez un type de service pour plus d'informations",

				"Web": "Notions avancées de [NGinX](https://nginx.org) et configuration simples de Apache.",
				"Mail": "Configuration poussées de Postfix (SMTP) et Dovecot (IMAP).",
				"Base de données": "Administration et utilisation de : Postgres, MariaDB, Redis et MongoDB",
				"VPN": "Configuration de Wireguard et OpenVPN."
			}
		},


		"Réseau": {

		}
	},

	"Projets": {
		"Web": {
			"LazyFox.fr": "Pure js. Voir [le code sur GitLab](git).",
			"Popcorn": "Svelte + NodeJS + FFMpeg + Postgres"
		}
	},

	"Contact": {
		"_text": "Vous pouvez me contacter [par mail](mailto:)."
	}
};