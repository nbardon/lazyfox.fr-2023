import {init_msgs, legals, tree} from './content.js';

const $ = (id) => document.getElementById(id);
String.prototype.removeAccents = function() { return this.normalize("NFD").replace(/[\u0300-\u036f]/g, ""); }

/////////////////////////////
// Console output

let printBulk = (txt, nowrap = false) => {
	return new Promise( resolv => {
		let p = document.createElement('p');
		$("console-output").append(p);
		
		if (nowrap)
			p.style.whiteSpace = "nowrap";

		let i = 0;
		let int = setInterval(() => {
			print(txt[i++], null, p);

			if (i >= txt.length) {
				clearInterval(int);
				resolv();
			}
		}, 20);
	});
};


let print = (txt, color, elem) => {
	txt = txt.replaceAll("<", "$lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br/>");
	txt = txt.replaceAll(/-{3,}/g, "<hr/>");
	txt = txt.replaceAll(/\*([^\*]+)\*/g, "<b>$1</b>");
	txt = txt.replaceAll(/_([^_]+)_/g, "<i>$1</i>");
	txt = txt.replaceAll(/\[([^\]]+)\]\(([^\)]+)\)/g, '<a href="$2" class="blue" target="_blank">$1</a>');
	txt = txt.replaceAll(/\{(red|blue|gold|white)\#([^\}]+)\}/g, '<span class="$1">$2</span>');

	if (elem == null) {
		elem = document.createElement("p");
		if (color != undefined && color != null)
			elem.classList.add(color);
		elem.innerHTML = txt;
		$("console-output").append(elem);
	} else
		elem.innerHTML += txt + '<br/>';


	setTimeout(() => $("console-output").scrollTo(0, $("console-output").scrollHeight), 20);
};


/////////////////////////////
// Console input

let autocomplete_list = {};

let updateAutocompleteList = () => {
	autocomplete_list = {};

	for (let cmd in commands)
		autocomplete_list[cmd] = cmd;

	for (let cat in menu_pointer) {
		if (cat[0] == '_')
			continue;
		
		autocomplete_list[cat.toLowerCase().removeAccents()] = cat;
	}
};

// n = number of chars (from the end) to animate
let autocomplete = (txt, n = 0, focus_at_the_end = true) => {
	return new Promise( resolv => {
		$("c-input").innerHTML = "";
		$("c-input").innerText = txt.substring(0, n);
		txt = txt.substring(n);

		let int = setInterval(() => {
			if (txt.length == 0) {
				clearInterval(int);
				if (focus_at_the_end){
					$("console-input").click(); // Cursor at the end of input
					console.log('aze');
				}
				$("autocomplete").innerText = "";
				resolv();
				return;
			}

			$("c-input").innerText += txt[0];
			txt = txt.substring(1);
		}, 15);
	});
};

$("autocomplete").addEventListener("click", e => {
	if (!e.target.hasAttribute("data-autocomplete"))
		return;

	autocomplete(e.target.getAttribute("data-autocomplete"), $("c-input").innerText.length);
});

let input_enter = () => {
	$("c-input").dispatchEvent(new KeyboardEvent("keydown", {keyCode:13}));
};

$("c-input").addEventListener("keydown", (e) => {
	$("autocomplete").innerText = "";

	if (e.keyCode == 13) { // Enter
		e.preventDefault();
		if (e.target.innerText.length == 0)
			return;

		if (!execute_command(e.target.innerText))
			menu_navigate(e.target.innerText);

		e.target.innerHTML = "";
		return;
	}

	if (e.keyCode == 9) { // Tab
		e.preventDefault();
		if (e.target.innerText.length == 0)
			return;

		for (let txt in autocomplete_list) {
			if (txt.startsWith(e.target.innerText.toLowerCase().removeAccents())) {
				autocomplete(autocomplete_list[txt], e.target.innerText.length);
				return;
			}
		}
		return;
	}
});

$("c-input").addEventListener("keyup", (e) => {
	if (e.target.innerText.length == 0)
		return;

	// Autocomplete
	for (let txt in autocomplete_list) {
		if (txt.startsWith(e.target.innerText.toLowerCase().removeAccents())) {
			$("autocomplete").innerText = autocomplete_list[txt].substring(e.target.innerText.length);
			$("autocomplete").setAttribute("data-autocomplete", autocomplete_list[txt])
			return;
		}
	}
});

/////////////////////////////
// Menu

let menu_ariane = "/";
let menu_pointer = tree;
let updateMenu = () => {
	let menu = $("menu");

	while (menu.children.length > 0)
		menu.children[0].remove();

	if (menu_pointer != tree) {
		let btn = document.createElement("button");
		btn.innerText = "..";
		btn.addEventListener("click", async () => {
			await autocomplete("..", 0, false);
			input_enter();
		});
		menu.append(btn);
	}

	for (let cat in menu_pointer) {
		if (cat[0] == '_')
			continue;

		let btn = document.createElement("button");
		btn.innerText = cat;
		btn.setAttribute("x-catname", cat);
		btn.addEventListener("click", async () => {
			await autocomplete(cat, 0, false);
			input_enter();
		});
		menu.append(btn);
	}

	updateAutocompleteList();
};

let menu_navigate = (catname) => {
	$("prompt").innerText = "";

	if (catname == '..') {
		menu_ariane = menu_ariane.substring(0,
							menu_ariane.lastIndexOf("/", menu_ariane.length-2) +1);
		
		menu_pointer = menu_resolv_path(menu_ariane);
	} else {
		if (!menu_pointer.hasOwnProperty(catname)) {
			print(`{red#Error : '${catname}' is not found}`, red);
			return;
		}

		if (typeof menu_pointer[catname] == "string") {
			print(`${catname} :`);
			print(menu_pointer[catname], "white");

			if (menu_pointer.hasOwnProperty("_prompt"))
				$("prompt").innerText = menu_pointer["_prompt"];

			return;
		}


		if (typeof menu_pointer[catname] == "object") {
			menu_pointer = menu_pointer[catname];
			menu_ariane += catname + '/';
			

			if (menu_pointer.hasOwnProperty('_goto')) {
				try {
					let goto_path = menu_pointer['_goto'];
					menu_pointer = menu_resolv_path(goto_path);
					menu_ariane = goto_path;
				} catch (err) {
					print(`${err.toString()} : ${menu_pointer['_goto']}`, "red");
				}
			}
		} else {
			let err = `"${menu_ariane+catname}" is not a string or an object.`;
			print(err, "red")
			throw err;
		}
	}



	updateMenu();
	print("---");
	print(`> Entering in {blue#${menu_ariane}}...`);

	if (menu_pointer.hasOwnProperty("_text"))
		print(menu_pointer["_text"], "white");

	if (menu_pointer.hasOwnProperty("_prompt"))
		$("prompt").innerText = menu_pointer["_prompt"];
};

let menu_resolv_path = (path) => {
	let pointer = tree;
	let parts = path.split("/");

	for (let p of parts) {
		if (p.length == 0)
			continue;

		if (!pointer.hasOwnProperty(p))
			throw Error("The path does not exist.");

		pointer = pointer[p];
	}

	return pointer;
};


/////////////////////////////
// Commands
let execute_command = (cmd) => {
	cmd = cmd.toLowerCase();

	if (cmd == "?") {
		cmd_help();
		return true;
	}

	if (commands.hasOwnProperty(cmd)) {
		commands[cmd]();
		return true;
	}

	return false;
};

let cmd_tree = () => {
	let result = [];

	let walk = (list = tree, prefix=" ", depth=0) => {
		if (depth > 10 || typeof list != "object")
			return;

		for (let elem in list) {
			if (elem[0] == '_')
				continue;


			result.push(`{white#${prefix}}/${elem}`);
			walk(list[elem], prefix + '    ', depth+1);
		}
	};

	walk();
	printBulk(result);
};

let cmd_help = () => {
	printBulk([
		"Commandes disponibles :",
		"- {gold#help} : Affiche ce message.",
		"- {gold#tree} : Affiche l'arborescence du menu.",
		"- {gold#legals} : Affiche les mentions légales."
		]);
};


let cmd_legals = () => {
	printBulk(legals);
};


let commands = {
	"help": cmd_help,
	"tree": cmd_tree,
	"legals": cmd_legals
};

/////////////////////////////
// UI

$("console-input").addEventListener("click", (e) => {
	let input = $("c-input");

	if (e.target == input)
		return;

	if (!input.hasChildNodes()) {
		input.focus();
		return;
	}

	const end = input.innerText.length;

	var range = document.createRange();
	range.setStart(input.childNodes[0], end);
	range.collapse(true);

	var sel = window.getSelection();
	sel.removeAllRanges();
	sel.addRange(range);
});



$("start").addEventListener("click", async (e) => {
	if (e.target.classList.contains('booting'))
		return;

	e.target.classList.add('booting');
	e.target.innerText = "Booting...";
	await printBulk(init_msgs, true);
	
	e.target.remove();
	updateMenu();
	$("prompt").innerText = menu_pointer["_prompt"];
});


$("legals").addEventListener("click", cmd_legals);